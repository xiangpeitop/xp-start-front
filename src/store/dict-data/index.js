import {getDictDataById} from "@/api/dict-data";

export default {
    namespaced: true,
    state: {
        tableData: [],
        dialogFormVisible: false,
        dialogTitle: "",
        pageSize: 10,
        currentPage: 1,
        total: 0,
        dictDataForm: {
            id: null,
            dictSort: "",
            dictLabel: "",
            dictValue: "",
            isDefault: "",
            dictStatus: "",
            dictTypeId: ""
        },
        multipleSelection: [],
    },
    getters: {},
    mutations: {
        openDialogForm(state, value) {
            state.dialogFormVisible = value.dialogFormVisible;
            state.dialogTitle = value.dialogTitle;
            //state.dictDataForm.dictTypeId = value.dictTypeId;
        },
        EditDictData(state, value) {
            state.dialogFormVisible = value.dialogFormVisible;
            state.dialogTitle = value.dialogTitle;
            Object.keys(state.dictDataForm).forEach((key) => {
                state.dictDataForm[key] = value.dictDataForm[key]
            })
        }
    },
    actions: {
        editDictData(state, id) {
            getDictDataById(id).then((res) => {
                var params = {
                    dialogFormVisible: true,
                    dialogTitle: "修改字典数据",
                    dictDataForm: res.data.data
                }
                state.commit("EditDictData", params);
            })
        }
    },
}
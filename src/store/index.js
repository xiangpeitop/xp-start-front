import Vue from 'vue'
import Vuex from 'vuex'
import menu from './menu'
import page from './page'
import role from './role'
import dictType from './dict-type'
import dictData from './dict-data'
import org from './org'
import sysTestUser from './test-user'
import user from './user'
import quartz from './quartz'
import upload from './upload'
import router from "@/router";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    // token
    token: '',
    // 验证码id
    verfyId: '',
    // 用户权限（菜单）
    permissions: [],
    dynamicRouters: JSON.parse(sessionStorage.getItem("dynamicRouters")),
    isFresh: '',
    // 动态导航标签默认值
    editableTabsValue: '/home',
    editableTabs:[],
    // editableTabs: [{
    //   title: "主页",
    //   name: "Welcome"
    // },],

  },
  getters: {
    // 防止刷新页面丢失当前tab
    // editableTabs: state => {
    //   if (sessionStorage.getItem("nowTab")) {
    //      state.editableTabs.push({title: '首页', name: '/Index'});
    //      state.editableTabs.push(JSON.parse(sessionStorage.getItem("nowTable")));
    //      return state.editableTabs;
    //   }
    // },
    // editableTabsValue: state => {
    //   if (sessionStorage.getItem("nowTabValue")) {
    //     return state.editableTabs = sessionStorage.getItem("nowTableValue");
    //   }
    // }
  },
  mutations: {
    addTab(state, tab) {
      // console.log("这是tab")
      // console.log(tab)
      sessionStorage.setItem("nowTab", JSON.stringify({title: tab.title, name: tab.name}));
      sessionStorage.setItem("nowTabValue", tab.name);
      let index = state.editableTabs.findIndex(e => e.name === tab.name)
      if(index === -1) {
        state.editableTabs.push({
          title: tab.title,
          name: tab.name,
        });
      }
      state.editableTabsValue = tab.name;
    },
    dynamicRouters(state, menus) {
      let routers = {
        path: '/index',
        name: 'Index',
        component: () => import('../views/IndexView.vue'),
        children: [
          // {
          //   path: '/welcome',
          //   name: 'Welcome',
          //   meta: {
          //     title: '主页'
          //   },
          //   component: () => import(/* webpackChunkName: "about" */ '../views/HomeView.vue')
          // },
        ]
      }
      if (menus) {
        menus.forEach((menu) => {
          if (menu.children.length > 0) {
            menu.children.forEach((item) => {
              // 菜单类型为1才是菜单，0是目录，2是按钮
              if (item.menuType === '1' && item.routerComponent) {
                let newRouter = {
                  path: item.menuPath,
                  name: item.routerName,
                  meta: {
                    title: item.menuName
                  },
                  component: () => import(`@/views/${item.routerComponent}.vue`)
                };
                routers.children.push(newRouter);
              }
            })
          } else {
            // 菜单类型为1才是菜单，0是目录，2是按钮
            if (menu.menuType === '1' && menu.routerComponent) {
              let newRouter = {
                path: menu.menuPath,
                name: menu.routerName,
                meta: {
                  title: menu.menuName
                },
                component: () => import(`@/views/${menu.routerComponent}.vue`)
              }
              routers.children.push(newRouter);
            }
          }
        })
        //this.$store.state.isFresh = "a";
        state.dynamicRouters = routers;
        state.isFresh = "xp"
        router.addRoute(routers)
        sessionStorage.setItem("doIt", "yes")
      }
    }
  },
  actions: {
  },
  modules: {
    menu: menu,
    page: page,
    role: role,
    dictType: dictType,
    dictData: dictData,
    user: user,
    quartz: quartz,
    sysTestUser: sysTestUser,
    upload: upload,
    org: org,
  }
})

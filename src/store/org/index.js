import {getSysOrgById} from "@/api/sysOrg";
import {getSysOrgs} from "@/api/sysOrg";

export default {
    namespaced: true,
    state: {
        dialogFormVisible: false,
        dialogTreeVisible: false,
        dialogTitle: '',
        pageSize: 10,
        currentPage: 1,
        total: 0,
        // 修改添加的表单数据
        sysOrgForm: {
            id: null,
            orgName: "" ,
            parentId: '0' ,
            parentName: '最高组织' ,
            orderNum: null ,
            leaderUserName: "" ,
            leaderPhone: "" ,
            status: null ,
            orgType: null ,
        },
        // 表格数据
        tableData: [],
        treeData: [],
        // 表格选中数据
        multipleSelection: [],
    },
    getters: {},
    mutations: {
        OpenTreeDialog(state, params) {
            state.dialogTreeVisible = params.dialogTreeVisible;
            state.treeData = params.treeData;
        },
        openDialogForm(state, value) {
            state.dialogFormVisible = value.dialogFormVisible;
            state.dialogTitle = value.dialogTitle;
        },
        EditSysOrg(state, value) {
            state.dialogFormVisible = value.dialogFormVisible;
            state.dialogTitle = value.dialogTitle;
            Object.keys(state.sysOrgForm).forEach((key) => {
                state.sysOrgForm[key] = value.sysOrgForm[key]
            })
        }
    },
    actions: {
        openTreeDialog(state, value) {
            getSysOrgs(null).then((res) => {
                var params = {
                    "treeData": res.data.data,
                    "dialogTreeVisible": value
                }
                state.commit("OpenTreeDialog", params)
            });

        },
        editSysOrg(state, id) {
            getSysOrgById(id).then((res) => {
                var params = {
                    dialogFormVisible: true,
                    dialogTitle: "修改",
                    sysOrgForm: res.data.data
                }
                state.commit("EditSysOrg", params);
            })
        }
    },
}

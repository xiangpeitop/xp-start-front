export default {
    namespaced: true,
    state: {
        currentPage: 1,
        pageSize: 10,
        total: 20
    },
    getters: {},
    mutations: {},
    actions: {},
}
import {getDictTypeById} from "@/api/dict-type";

export default {
    namespaced: true,
    state: {
        tableData: [],
        dialogFormVisible: false,
        dialogTitle: "",
        pageSize: 10,
        currentPage: 1,
        total: 0,
        dictTypeForm: {
            id: null,
            dictSort: "",
            dictTypeName: "",
            dictTypeKey: "",
            dictStatus: "",
            remark: ""
        },
        multipleSelection: [],
    },
    getters: {},
    mutations: {
        openDialogForm(state, value) {
            state.dialogFormVisible = value.dialogFormVisible;
            state.dialogTitle = value.dialogTitle;
        },
        EditDictType(state, value) {
            state.dialogFormVisible = value.dialogFormVisible;
            state.dialogTitle = value.dialogTitle;
            Object.keys(state.dictTypeForm).forEach((key) => {
                state.dictTypeForm[key] = value.dictTypeForm[key]
            })
        }
    },
    actions: {
        editDictType(state, id) {
            getDictTypeById(id).then((res) => {
                var params = {
                    dialogFormVisible: true,
                    dialogTitle: "修改字典类型",
                    dictTypeForm: res.data.data
                }
                state.commit("EditDictType", params);
            })
        },
    },
}
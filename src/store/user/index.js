import {getUserById,getUserRoleByUserId} from "@/api/user";
import {getRoles} from "@/api/role";
import {getSysOrgs} from "@/api/sysOrg";

export default {
    namespaced: true,
    state: {
        userId: null,
        // 所有角色
        roles: [],
        treeData: [],
        // 用户已有角色
        hasRoles: {},
        selected: [],
        // 分配的角色
        userRoles: [],
        tableData: [],
        dialogFormVisible: false,
        dialogFormVisible1: false,
        dialogVisible: false,
        dialogTreeVisible: false,
        dialogTitle: "",
        pageSize: 10,
        currentPage: 1,
        total: 0,
        userForm: {
            id: null,
            userName: "",
            nickName: "",
            email: "",
            phoneNumber: "",
            password: "",
            avatar: "",
            status: "",
            tenantId: "",
            sex: "",
            belongOrg: null,
            belongOrgName: "",
        },
        multipleSelection: [],
    },
    getters: {},
    mutations: {
        OpenTreeDialog(state, params) {
            state.dialogTreeVisible = params.dialogTreeVisible;
            state.treeData = params.treeData;
        },
        openDialogForm(state, value) {
            state.dialogFormVisible = value.dialogFormVisible;
            state.dialogTitle = value.dialogTitle;
        },
        EditUser(state, value) {
            state.dialogFormVisible = value.dialogFormVisible;
            state.dialogTitle = value.dialogTitle;
            Object.keys(state.userForm).forEach((key) => {
                state.userForm[key] = value.userForm[key]
            })
        },
        EditUserBySelf(state, value) {
            state.dialogFormVisible1 = value.dialogFormVisible1;
            state.dialogTitle = value.dialogTitle;
            Object.keys(state.userForm).forEach((key) => {
                state.userForm[key] = value.userForm[key]
            })
        },
        OpenUserRoleDialog(state, params) {
            state.dialogVisible = params.dialogVisible,
            state.roles = params.roles;
            state.hasRoles = params.hasRoles;
            state.userId = params.userId;
            state.selected = params.hasRoles.roleIds === null ? [] : params.hasRoles.roleIds ;
        },
    },
    actions: {
        openTreeDialog(state, value) {
            getSysOrgs(null).then((res) => {
                var params = {
                    "treeData": res.data.data,
                    "dialogTreeVisible": value
                }
                state.commit("OpenTreeDialog", params)
            });

        },
        editUser(state, id) {
            getUserById(id).then((res) => {
                var params = {
                    dialogFormVisible: true,
                    dialogTitle: "修改用户",
                    userForm: res.data.data
                }
                state.commit("EditUser", params);
            })
        },
        editUserBySelf(state, id) {
            getUserById(id).then((res) => {
                var params = {
                    dialogFormVisible1: true,
                    dialogTitle: "修改用户",
                    userForm: res.data.data
                }
                state.commit("EditUserBySelf", params);
            })
        },
        assignRole(state, params) {
            getRoles(null).then((res) => {
                getUserRoleByUserId(params).then((res1) => {
                    var allRole = res.data.data;
                    var hasRole = res1.data.data
                    var data = {
                        roles: allRole,
                        userId: params,
                        dialogVisible: true,
                        hasRoles: hasRole
                    }
                    state.commit("OpenUserRoleDialog", data);
                });
            });
        }
    },
}
import {getRoleById, getPermissionByRole} from "@/api/role";
import {getMenuTree} from "@/api/menu";

export default {
    namespaced: true,
    state: {
        // 角色权限id
        id: null,
        roleId: null,
        // 菜单数据
        treeData: [],
        // 已经有的权限
        rolePermissions: {},
        // 分配的权限
        assignPermission: [],
        tableData: [],
        dialogFormVisible: false,
        dialogTreeVisible: false,
        dialogTitle: "",
        pageSize: 10,
        currentPage: 1,
        total: 0,
        roleForm: {
            id: null,
            roleName: "",
            roleKey: "",
            orderNum: "",
            status: "",
            remark: ""
        },
        multipleSelection: [],
    },
    getters: {},
    mutations: {
        openDialogForm(state, value) {
            state.dialogFormVisible = value.dialogFormVisible;
            state.dialogTitle = value.dialogTitle;
        },
        EditRole(state, value) {
            state.dialogFormVisible = value.dialogFormVisible;
            state.dialogTitle = value.dialogTitle;
            Object.keys(state.roleForm).forEach((key) => {
                state.roleForm[key] = value.roleForm[key]
            })
        },
        OpenRoleTreeDialog(state, params) {
            state.roleId = params.roleId;
            state.dialogTreeVisible = params.dialogTreeVisible,
            state.treeData = params.treeData;
            state.rolePermissions = params.rolePermissions;
            state.assignPermission = params.rolePermissions.menuIds;
        }
    },
    actions: {
        editRole(state, id) {
            getRoleById(id).then((res) => {
                var params = {
                    dialogFormVisible: true,
                    dialogTitle: "修改角色",
                    roleForm: res.data.data
                }
                state.commit("EditRole", params);
            })
        },
        openTreeDialog(state, params) {
            getMenuTree(null).then((res) => {
                getPermissionByRole(params).then((res1) => {
                    // var allMenu = res.data.data;
                    // var hasMenu = res1.data.data
                    // console.log(hasMenu)
                    // allMenu.forEach((menu) => {
                    //     if (hasMenu.includes(menu.id)) {
                    //         menu["has"] = true
                    //     } else {
                    //         menu["has"] = false
                    //     }
                    // })
                    // console.log(allMenu);
                    var data = {
                        treeData: res.data.data,
                        roleId: params,
                        dialogTreeVisible: true,
                        rolePermissions: res1.data.data,
                    }
                    state.commit("OpenRoleTreeDialog", data);
                });
            });


        }
    },
}
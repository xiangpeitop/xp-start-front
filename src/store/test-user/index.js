import {getSysTestUserById} from "@/api/sysTestUser";

export default {
    namespaced: true,
    state: {
        dialogFormVisible: false,
        dialogTitle: '',
        pageSize: 10,
        currentPage: 1,
        total: 0,
        // 修改添加的表单数据
        sysTestUserForm: {
            name: "" ,
            age: null ,
            hobby: "" ,
        },
        // 定时任务表格数据
        tableData: [],
        // 表格选中数据
        multipleSelection: [],
    },
    getters: {},
    mutations: {
        openDialogForm(state, value) {
            state.dialogFormVisible = value.dialogFormVisible;
            state.dialogTitle = value.dialogTitle;
        },
        EditSysTestUser(state, value) {
            state.dialogFormVisible = value.dialogFormVisible;
            state.dialogTitle = value.dialogTitle;
            state.sysTestUserForm = value.sysTestUserForm;
        }
    },
    actions: {
        editSysTestUser(state, id) {
            getSysTestUserById(id).then((res) => {
                var params = {
                    dialogFormVisible: true,
                    dialogTitle: "修改测试用户",
                    sysTestUserForm: res.data.data
                }
                state.commit("EditSysTestUser", params);
            })
        }
    },
}
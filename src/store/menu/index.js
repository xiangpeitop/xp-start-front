import {getMenus, getMenuById} from "@/api/menu";

export default {
    namespaced: true,
    state: {
        dialogFormVisible: false,
        dialogTreeVisible: false,
        iconDialogVisible: false,
        dialogTitle: '',
        pageSize: 10,
        currentPage: 1,
        total: 0,
        // 修改添加的表单数据
        menuForm: {
            id: null,
            menuName: '',
            menuIcon: '',
            menuPath: null,
            routerName: null,
            routerComponent: null,
            permission: '',
            menuType: '',
            orderNum: null,
            parentId: '0',
            parentName: '主目录',
            status: '',
        },
        selectedIcon: '',
        // 添加修改时的父级选择数据
        treeData: [],
        // 菜单表格数据
        tableData: [],
        // 菜单表格选中数据
        multipleSelection: [],
    },
    getters: {},
    mutations: {
        openDialogForm(state, value) {
            state.dialogFormVisible = value.dialogFormVisible;
            state.menuForm.parentId = value.parentId;
            state.menuForm.parentName = value.parentName;
            state.dialogTitle = value.dialogTitle;
        },
        closeDialogForm(state, params) {
            state.dialogFormVisible = params;
        },
        OpenTreeDialog(state, params) {
            state.dialogTreeVisible = params.dialogTreeVisible;
            state.treeData = params.treeData;
        },
        EditMenu(state, params) {
            state.selectedIcon = params.menuForm.menuIcon;
            state.dialogFormVisible = params.dialogTreeVisible;
            state.dialogTitle = params.dialogTitle;
            Object.keys(state.menuForm).forEach((key) => {
                state.menuForm[key] = params.menuForm[key]
            })
        }
    },
    actions: {
        openTreeDialog(state, value) {
            getMenus(null).then((res) => {
                var params = {
                    "treeData": res.data.data,
                    "dialogTreeVisible": value
                }
                state.commit("OpenTreeDialog", params)
            });

        },
        editMenu(state, id) {
            getMenuById(id).then((res) => {
                var params = {
                    menuForm: res.data.data,
                    dialogTreeVisible: true,
                    dialogTitle: "修改菜单"
                };
                state.commit("EditMenu", params);
            })
        },
    },
}

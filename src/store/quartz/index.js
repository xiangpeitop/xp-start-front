import {getQuartzById} from "@/api/quartz";

export default {
    namespaced: true,
    state: {
        dialogFormVisible: false,
        dialogTitle: '',
        pageSize: 10,
        currentPage: 1,
        total: 0,
        // 修改添加的表单数据
        quartzForm: {
            id: null,
            jobGroup: '',
            jobKey: '',
            jobStatus: '',
            cronExpress: '',
            remark: '',
            jobClass: ''
        },
        // 定时任务表格数据
        tableData: [],
        // 表格选中数据
        multipleSelection: [],
    },
    getters: {},
    mutations: {
        openDialogForm(state, value) {
            state.dialogFormVisible = value.dialogFormVisible;
            state.dialogTitle = value.dialogTitle;
        },
        closeDialogForm(state, params) {
            state.dialogFormVisible = params;
        },
        EditQuartz(state, params) {
            state.dialogFormVisible = params.dialogFormVisible;
            state.dialogTitle = params.dialogTitle;
            Object.keys(state.quartzForm).forEach((key) => {
                state.quartzForm[key] = params.quartzForm[key]
            })
        }
    },
    actions: {
        editQuartz(state, id) {
            getQuartzById(id).then((res) => {
                var params = {
                    quartzForm: res.data.data,
                    dialogFormVisible: true,
                    dialogTitle: "修改定时任务"
                };
                state.commit("EditQuartz", params);
            })
        },
    },
}
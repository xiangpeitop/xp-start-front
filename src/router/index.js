import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Login',
    component: () => import(/* webpackChunkName: "about" */ '../views/LoginView.vue')
  },
]


const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: routes
})

router.beforeEach((to, from, next) => {
  if (store.state.isFresh === '') {
    store.commit("dynamicRouters", JSON.parse(sessionStorage.getItem("permissions")))
    if (to.name === "Login") {
      next();
    }
    else if (router.options.routes.length === 1) {
      next({ ...to, replace: true })
    }
  }
next()
})

// function createRouter(item) {
//     let newRouter = {
//       path: item.menuPath,
//       name: item.routerName,
//       meta: {
//         title: item.menuName
//       },
//       component: () => import(`@/views/${item.routerComponent}.vue`)
//     }
//     //newRouter['component'] = () => import(`@/views/${item.routerComponent}.vue`)
//     return newRouter;
// }

export default router

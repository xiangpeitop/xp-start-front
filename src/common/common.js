import { Notification } from 'element-ui';

// element通知
export const notify = (type, msg) => {
    return Notification({
        // title: '',
        type: type,
        dangerouslyUseHTMLString: true,
        message: msg
    });
}

// 获取网络图片的宽高
export const getNetWorkImgWH = (url) => {
    return new Promise((resolve, reject) => {
        const img = new Image();

        img.onload = function() {
            resolve({
                width: img.width,
                height: img.height
            });
        };

        img.onerror = function() {
            reject(new Error('无法加载图片'));
        };

        img.src = url;
    });
}

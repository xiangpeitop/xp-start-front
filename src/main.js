import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from './api/request'


import {
    Button, Select, Row, Link, Message, Col, Menu, MenuItem,
    Container, Aside, Main, Header, Dropdown, DropdownItem, DropdownMenu,
    Submenu, Table, TableColumn, Breadcrumb, BreadcrumbItem,
    Tabs, TabPane, MenuItemGroup, Divider, Dialog, Form, FormItem,
    Option, Input, RadioGroup, Radio, Footer, Pagination, Checkbox,
    Tree, MessageBox, CheckboxGroup, Icon, Avatar, Upload,Image, Switch, Popover
  } from 'element-ui';

Vue.component(Popover.name, Popover);
Vue.component(Switch.name, Switch);
Vue.component(Image.name, Image);
Vue.component(Upload.name, Upload);
Vue.component(DropdownMenu.name, DropdownMenu);
Vue.component(Avatar.name, Avatar);
Vue.component(Icon.name, Icon);
Vue.component(MessageBox.name, MessageBox);
Vue.component(CheckboxGroup.name, CheckboxGroup);
Vue.component(Tree.name, Tree);
Vue.component(Checkbox.name, Checkbox);
Vue.component(Pagination.name, Pagination);
Vue.component(Footer.name, Footer);
Vue.component(Input.name, Input);
Vue.component(RadioGroup.name, RadioGroup);
Vue.component(Radio.name, Radio);
Vue.component(Option.name, Option);
Vue.component(Divider.name, Divider);
Vue.component(Form.name, Form);
Vue.component(Dialog.name, Dialog);
Vue.component(FormItem.name, FormItem);
Vue.component(MenuItemGroup.name, MenuItemGroup);
Vue.component(Tabs.name, Tabs);
Vue.component(TabPane.name, TabPane);
Vue.component(Button.name, Button);
Vue.component(Select.name, Select);
Vue.component(Row.name, Row);
Vue.component(Link.name, Link);
Vue.component(Message.name, Message);
Vue.component(Col.name, Col);
Vue.component(Menu.name, Menu);
Vue.component(MenuItem.name, MenuItem);
Vue.component(Container.name, Container);
Vue.component(Aside.name, Aside);
Vue.component(Main.name, Main);
Vue.component(Header.name, Header);
Vue.component(Dropdown.name, Dropdown);
Vue.component(DropdownItem.name, DropdownItem);
Vue.component(Submenu.name, Submenu);
Vue.component(Table.name, Table);
Vue.component(TableColumn.name, TableColumn);
Vue.component(Breadcrumb.name, Breadcrumb);
Vue.component(BreadcrumbItem.name, BreadcrumbItem);

Vue.prototype.$http = axios
Vue.prototype.$message = Message;
Vue.prototype.$confirm = MessageBox.confirm;
Vue.config.productionTip = false

// websocket
Vue.prototype.$websocket = null;
// 全局方法
Vue.prototype.$getButtonAuth = (permission) => {
    let permissions = JSON.parse(sessionStorage.getItem("button-permissions"));
    return permissions.includes(permission);
}

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

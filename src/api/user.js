import axios from "./request";

// 获取用户分页
export const getUsers = (params) => {
    return axios({
        url: "/sys-user/page",
        method: "GET",
        params: params
    })
}

// 通过id获取用户
export const getUserById = (params) => {
    return axios({
        url: "/sys-user/" + params,
        method: "GET"
    })
}

// 通过id删除用户
export const deleteUserById = (params) => {
    return axios({
        url: "/sys-user/" + params,
        method: "DELETE"
    })
}

// 批量删除用户
export const deleteUserByIds = (params) => {
    return axios({
        url: "/sys-user/batch",
        method: "DELETE",
        data: params
    })
}

// 修改用户
export const editUser = (params) => {
    return axios({
        url: "/sys-user/",
        method: "PUT",
        data: params
    })
}

// 修改密码
export const resetPwd = (params) => {
    return axios({
        url: "/sys-user/pwd",
        method: "PUT",
        data: params
    })
}

// 修改用户状态
export const editUserStatus = (params) => {
    return axios({
        url: "/sys-user/status",
        method: "PUT",
        data: params
    })
}

// 添加用户
export const addUser = (params) => {
    return axios({
        url: "/sys-user/",
        method: "POST",
        data: params
    })
}

// 为用户分配角色
export const addUserRole = (params) => {
    return axios({
        url: "/sys-user-role/",
        method: "POST",
        data: params
    })
}

// 通过用户id查找已分配的角色
export const getUserRoleByUserId = (params) => {
    return axios({
        url: "/sys-user-role/role/" + params,
        method: "GET",
    })
}


// 获取用户权限
export const getAuth = (params) => {
    return axios({
        url: "/auth",
        method: "GET",
        params: params
    })
}


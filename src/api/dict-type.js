import axios from "./request";

// 获取字典类型分页
export const getDictTypes = (params) => {
    return axios({
        url: "/sys-dict-type/page",
        method: "GET",
        params: params
    })
}

// 通过id获取字典类型
export const getDictTypeById = (params) => {
    return axios({
        url: "/sys-dict-type/" + params,
        method: "GET"
    })
}

// 通过id删除字典
export const deleteDictTypeById = (params) => {
    return axios({
        url: "/sys-dict-type/" + params,
        method: "DELETE"
    })
}

// 批量删除字典
export const deleteDictTypeByIds = (params) => {
    return axios({
        url: "/sys-dict-type/batch",
        method: "DELETE",
        data: params
    })
}

// 修改字典
export const editDictType = (params) => {
    return axios({
        url: "/sys-dict-type/",
        method: "PUT",
        data: params
    })
}

// 添加字典
export const addDictType = (params) => {
    return axios({
        url: "/sys-dict-type/",
        method: "POST",
        data: params
    })
}

// 字典列表
export const typeList = (params) => {
    return axios({
        url: "/sys-dict-type/list",
        method: "GET",
        params: params
    })
}

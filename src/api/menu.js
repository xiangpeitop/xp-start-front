import axios from "./request";

// 添加菜单
export const addMenu = (params) => {
    return axios({
        url: "/sys-menu/",
        method: "post",
        data: params
    })
}

// 获取菜单分页
export const getMenus = (params) => {
    //console.log(params)
    return axios({
        url: "/sys-menu/page",
        method: "get",
        params: params
    })
}

// 获取菜单列表
export const getMenuList = () => {
    //console.log(params)
    return axios({
        url: "/sys-menu/list",
        method: "get",
    })
}

export const getMenuTree = () => {
    return axios({
        url: "/sys-menu/tree",
        method: "get"
    })
}

// 通过id获取菜单
export const getMenuById = (params) => {
    return axios({
        url: "/sys-menu/" + params,
        method: "get"
    })
}

// 通过id删除菜单
export const deleteMenuById = (params) => {
    //console.log(params)
    return axios({
        url: "/sys-menu/" + params,
        method: "delete"
    })
}

// 批量删除菜单
export const deleteMenuByIds = (params) => {
    //console.log(params)
    return axios({
        url: "/sys-menu/batch",
        method: "delete",
        data: params
    })
}

// 修改菜单
export const editMenu = (params) => {
    return axios({
        url: "/sys-menu/",
        method: "put",
        data: params
    })
}
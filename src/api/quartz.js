import axios from "./request";


// 立即执行一次
export const executeOnce = (params) => {
    return axios({
        url: "/sys-job/execute/once",
        method: "post",
        data: params
    })
}

// 停用任务
export const stopQuartz = (params) => {
    return axios({
        url: "/sys-job/pause/job",
        method: "post",
        data: params
    })
}

// 启用任务
export const startQuartz = (params) => {
    return axios({
        url: "/sys-job/start/job",
        method: "post",
        data: params
    })
}

// 添加任务
export const addQuartz = (params) => {
    return axios({
        url: "/sys-job/",
        method: "post",
        data: params
    })
}

// 获取任务列表
export const getQuartzs = (params) => {
    //console.log(params)
    return axios({
        url: "/sys-job/page",
        method: "get",
        params: params
    })
}

// 通过id获取任务
export const getQuartzById = (params) => {
    return axios({
        url: "/sys-job/" + params,
        method: "get"
    })
}

// 通过id删除任务
export const deleteQuartzById = (params) => {
    //console.log(params)
    return axios({
        url: "/sys-job/" + params,
        method: "delete"
    })
}

// 批量删除任务
export const deleteQuartzByIds = (params) => {
    //console.log(params)
    return axios({
        url: "/sys-job/batch",
        method: "delete",
        data: params
    })
}

// 修改任务
export const editQuartz = (params) => {
    return axios({
        url: "/sys-job/",
        method: "put",
        data: params
    })
}
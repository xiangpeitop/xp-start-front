import axios from "./request";

// 获取测试用户分页
export const getSysTestUsers = (params) => {
    return axios({
        url: "/sys-test-user/page",
        method: "GET",
        params: params
    })
}

// 通过id获取测试用户
export const getSysTestUserById = (params) => {
    return axios({
        url: "/sys-test-user/" + params,
        method: "GET"
    })
}

// 通过id删除测试用户
export const deleteSysTestUserById = (params) => {
    return axios({
        url: "/sys-test-user/" + params,
        method: "DELETE"
    })
}

// 批量删除测试用户
export const deleteSysTestUserByIds = (params) => {
    return axios({
        url: "/sys-test-user/batch",
        method: "DELETE",
        data: params
    })
}

// 修改测试用户
export const editSysTestUser = (params) => {
    return axios({
        url: "/sys-test-user/",
        method: "PUT",
        data: params
    })
}

// 添加测试用户
export const addSysTestUser = (params) => {
    return axios({
        url: "/sys-test-user/",
        method: "POST",
        data: params
    })
}

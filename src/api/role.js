import axios from "./request";

// 获取角色分页
export const getRoles = (params) => {
    return axios({
        url: "/sys-role/page",
        method: "GET",
        params: params
    })
}

// 通过id获取角色
export const getRoleById = (params) => {
    return axios({
        url: "/sys-role/" + params,
        method: "GET"
    })
}

// 通过id删除角色
export const deleteRoleById = (params) => {
    return axios({
        url: "/sys-role/" + params,
        method: "DELETE"
    })
}

// 批量删除角色
export const deleteRoleByIds = (params) => {
    return axios({
        url: "/sys-role/batch",
        method: "DELETE",
        data: params
    })
}

// 修改角色
export const editRole = (params) => {
    return axios({
        url: "/sys-role/",
        method: "PUT",
        data: params
    })
}

// 添加角色
export const addRole = (params) => {
    return axios({
        url: "/sys-role/",
        method: "POST",
        data: params
    })
}

// 为角色添加权限
export const addPermission = (params) => {
    return axios({
        url: "/sys-role-menu/",
        method: "POST",
        data: params
    })
}

// 根据角色id查找权限
export const getPermissionByRole = (params) => {
    return axios({
        url: "/sys-role-menu/role/" + params,
        method: "GET"
    })
}
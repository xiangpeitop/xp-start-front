import axios from "./request";

// 获取分页
export const getSysOpLogs = (params) => {
    return axios({
        url: "/sys-op-log/page",
        method: "POST",
        data: params
    })
}

// 获取列表
export const getSysOpLogList = () => {
    return axios({
        url: "/sys-op-log/list",
        method: "GET"
    })
}

// 通过id获取
export const getSysOpLogById = (params) => {
    return axios({
        url: "/sys-op-log/" + params,
        method: "GET"
    })
}

// 通过id删除
export const deleteSysOpLogById = (params) => {
    return axios({
        url: "/sys-op-log/" + params,
        method: "DELETE"
    })
}

// 批量删除
export const deleteSysOpLogByIds = (params) => {
    return axios({
        url: "/sys-op-log/batch",
        method: "DELETE",
        data: params
    })
}

// 修改
export const editSysOpLog = (params) => {
    return axios({
        url: "/sys-op-log/",
        method: "PUT",
        data: params
    })
}

// 添加
export const addSysOpLog = (params) => {
    return axios({
        url: "/sys-op-log/",
        method: "POST",
        data: params
    })
}

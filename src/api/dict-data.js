import axios from "./request";

// 获取字典数据分页
export const getDictDatas = (params) => {
    return axios({
        url: "/sys-dict-data/page",
        method: "GET",
        params: params
    })
}

// 通过id获取字典数据
export const getDictDataById = (params) => {
    return axios({
        url: "/sys-dict-data/" + params,
        method: "GET"
    })
}

// 通过id删除字典数据
export const deleteDictDataById = (params) => {
    return axios({
        url: "/sys-dict-data/" + params,
        method: "DELETE"
    })
}

// 批量删除字典数据
export const deleteDictDataByIds = (params) => {
    return axios({
        url: "/sys-dict-data/batch",
        method: "DELETE",
        data: params
    })
}

// 修改字典数据
export const editDictData = (params) => {
    return axios({
        url: "/sys-dict-data/",
        method: "PUT",
        data: params
    })
}

// 添加字典数据
export const addDictData = (params) => {
    return axios({
        url: "/sys-dict-data/",
        method: "POST",
        data: params
    })
}

// 通过字典类型查找数据
export const getDictDataByType = (params) => {
    return axios({
        url: "/sys-dict-data/type/" + params.dictTypeId,
        method: "GET",
        params: {
            current: params.current,
            size: params.size
        }
    })
}

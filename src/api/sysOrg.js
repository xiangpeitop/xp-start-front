import axios from "./request";

// 获取分页
export const getSysOrgs = (params) => {
    return axios({
        url: "/sys-org/page",
        method: "GET",
        params: params
    })
}

// 获取列表
export const getSysOrgList = () => {
    return axios({
        url: "/sys-org/list",
        method: "GET"
    })
}

// 通过id获取
export const getSysOrgById = (params) => {
    return axios({
        url: "/sys-org/" + params,
        method: "GET"
    })
}

// 通过id删除
export const deleteSysOrgById = (params) => {
    return axios({
        url: "/sys-org/" + params,
        method: "DELETE"
    })
}

// 批量删除
export const deleteSysOrgByIds = (params) => {
    return axios({
        url: "/sys-org/batch",
        method: "DELETE",
        data: params
    })
}

// 修改
export const editSysOrg = (params) => {
    return axios({
        url: "/sys-org/",
        method: "PUT",
        data: params
    })
}

// 添加
export const addSysOrg = (params) => {
    return axios({
        url: "/sys-org/",
        method: "POST",
        data: params
    })
}

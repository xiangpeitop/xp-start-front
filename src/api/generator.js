import axios from "./request";

// 获取所有表名
export const getTables = (params) => {
    return axios({
        url: "/sys-table/",
        method: "get",
        params: params
    })
}

// 获取表字段
export const getTableColumns = (params) => {
    return axios({
        url: "/sys-table/columns",
        method: "get",
        params: params
    })
}

// 获取表字段
export const getTableColumnsSelect = (params) => {
    return axios({
        url: "/sys-table/columns/select",
        method: "get",
        params: params
    })
}

// 生成代码
export const generateCode = (params) => {
    return axios({
        url: "/sys-table/generator",
        method: "post",
        data: params
    })
}

import axios from "./request";

// 获取验证码
export const getVerfyCode = () => {
    return axios({
        url: "/captcha",
        method: "get"
    })
}

// 登录请求
export const login = (params) => {
    return axios({
        url: "/xpstart/login",
        method: "post",
        data: params
    })
}

// 退出登录
export const logout = () => {
    return axios({
        url: "/logout",
        method: "post",
    })
}